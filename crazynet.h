/* Weditres generated include file. Do NOT edit */

// All defines
#include <windows.h>
#include <winsock.h>
#include <string.h>


// Main Window
#define	_APS_NEXT_CONTROLVALUE	1000
#define	IDR_MENU	1020
#define	IDI_MYICON	1030
#define	IDC_MAIN_MDI	1040
#define	IDC_CHILD_EDIT	1050
#define	IDC_MAIN_EDIT	1260
#define	IDC_MAIN_TOOL	1270
#define	IDC_MAIN_STATUS	1280
#define	IDB_CRAZYNET	1060

// New
#define	ID_VHOST_NEW	1061
#define	ID_VNEW		1062
#define ID_VIPNEW	1009
#define ID_VHNNEW	1008

// Update
#define	ID_VHOST_UPDATE	1162
#define	ID_VUPDATE	1080
#define	IPUPDATE	1090
#define	HNUPDATE	1100

// Ie defines
#define	IDNECTARINE	1110
#define	IDCRAZYNET	1120
#define	IDINSANE	1130

// Preference
#define	ID_CONF_MOD	1140
#define	ID_CONFSAVE	1150
#define	IDCONFEMAIL	1290
#define	ID_CONFPASS	1300
#define	ID_CONFEMAIL	1310

// Del
#define	ID_VHOST_DEL	1161
#define	ID_VDEL	1166
#define ID_DELL	1179

// Menu defines
#define	ID_FILE_EXIT	1170
#define	ID_FILE_CONF	1180
#define	ID_EDIT_CUT	1190
#define	ID_EDIT_COPY	1200
#define	ID_EDIT_PASTE	1210
#define	ID_EDIT_ERASE	1220
#define	ID_HELP_HELP	1230
#define	ID_HELP_ABOUT	1240

// Debug defines
#define	_APS_NEXT_COMMAND_VALUE	40005
#define PRINTERROR(s)	\
		MessageBox(hwnd, "\n%: %d\n", s, WSAGetLastError())

// Socket defines
#define CLIENt ("CrazyNet DynDNS")
#define CLIENtV ("v0.0.1")
#define DEST_IP 	"195.62.234.69"
#define DEST_PASS	"testt"

